package StockExchangeSimulator;

import java.util.ArrayList;

public class ExchangeThread implements Runnable{
    private ArrayList<Share> shares;
    private ArrayList<Purchaser> purchasers;

    public ExchangeThread(ArrayList<Share> shares, ArrayList<Purchaser> purchasers) {
        this.shares = shares;
        this.purchasers = purchasers;
    }

    @Override
    public void run() {
        for(Share share : shares){
            Thread threadShare = new Thread(new ChangePriceThread(share));
            threadShare.start();
        }
        for(Purchaser purchaser : purchasers){
            Thread threadShare = new Thread(new BuyShareThread(purchaser, shares));
            threadShare.start();
        }
    }
}
