package StockExchangeSimulator;

import java.util.ArrayList;

public class Purchaser {
    private String name;
    private ArrayList<Share> requiredShares;

    private ArrayList<Share> boughtShares = new ArrayList<>();

    public Purchaser(String name, ArrayList<Share> requiredShares) {
        this.name = name;
        this.requiredShares = requiredShares;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Share> getRequiredShares() {
        return requiredShares;
    }

    public ArrayList<Share> getBoughtShares() {
        return boughtShares;
    }

    public void addBoughtShare(Share share) {
        boughtShares.add(share);
    }
}
