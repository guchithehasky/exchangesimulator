package StockExchangeSimulator;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class BuyShareThread implements Runnable{
    private Purchaser purchaser;
    private ArrayList<Share> shares;
    public BuyShareThread(Purchaser purchaser, ArrayList<Share> shares) {
        this.purchaser = purchaser;
        this.shares = shares;
    }

    @Override
    public void run() {
        while(true) {
            for (Share requiredShare : purchaser.getRequiredShares()) {
                for (Share share : shares) {
                    if (share.getName().equals(requiredShare.getName())) {
                        LocalDateTime dateTime = LocalDateTime.now();
                        String formattedDate = dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                        if (requiredShare.getPrice() == share.getPrice() &&
                            share.getAmount() > requiredShare.getAmount()) {
                            share.minusAmount(requiredShare.getAmount());
                            purchaser.addBoughtShare(requiredShare);
                            System.out.printf("\n%s Attempt to buy shares %s for %s successful. Bought %d shares",
                                    formattedDate, share.getName(), purchaser.getName(), requiredShare.getAmount());
                        } else {
                            System.out.printf("\n%s Attempt to buy shares %s for %s isn't successful", formattedDate,
                                    share.getName(), purchaser.getName());
                        }
                    }
                }
            }
            try {
                Thread.sleep(1000*5);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}