package StockExchangeSimulator;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ChangePriceThread implements Runnable{
    private Share share;

    public ChangePriceThread(Share share) { // Конструктор
        this.share = share;
    }

    @Override
    public void run() {
        while(true) {
            try {
                Thread.sleep(1000*30);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            LocalDateTime dateTime = LocalDateTime.now();
            String formattedDate = dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            share.changePrice();
            System.out.printf("\n%s Price of share %s changed. Now is: %d",
            formattedDate, share.getName(), share.getPrice());
        }
    }
}
