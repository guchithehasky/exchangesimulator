package StockExchangeSimulator;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Share> shares = new ArrayList<>();
        ArrayList<Purchaser> purchasers = new ArrayList<>();

        shares.add(new Share("APPL", 100, 141));
        shares.add(new Share("COKE", 1000, 387));
        shares.add(new Share("IBM", 200, 137));

        ArrayList<Share> reqSharesAlice = new ArrayList<>();
        reqSharesAlice.add(new Share("APPL", 10, 137));
        reqSharesAlice.add(new Share("COKE", 20, 364));
        purchasers.add(new Purchaser("Alice", reqSharesAlice));

        ArrayList<Share> reqSharesBob = new ArrayList<>();
        reqSharesBob.add(new Share("APPL", 10, 145));
        reqSharesBob.add(new Share("IBM", 20, 137));
        purchasers.add(new Purchaser("Bob", reqSharesBob));

        ArrayList<Share> reqSharesCharlie = new ArrayList<>();
        reqSharesCharlie.add(new Share("COKE", 30, 398));
        purchasers.add(new Purchaser("Charlie", reqSharesCharlie));

        Thread threadShare = new Thread(new ExchangeThread(shares, purchasers));
        System.out.println("\n\tExchange work");
        threadShare.start();
        try {
            Thread.sleep(1000*60*10);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        // Вивід фінальної статистики
        System.out.println("\n\n\n\tExchange close");
        System.out.println("\n\tPrice share:\n");
        for(Share share : shares){
            System.out.printf("\tCompany %s \tPrice: %d \tAmount: %d\n", share.getName(),
                    share.getPrice(), share.getAmount());
        }
        System.out.println("\n\tResult");
        for(Purchaser purchaser : purchasers){
            System.out.printf("\nPurchaser %s:", purchaser.getName());
            for(Share share : purchaser.getBoughtShares()){
                System.out.printf("\n Attempt to buy shares %s for %s successful. Bought %d shares", share.getName(),
                        purchaser.getName(), share.getAmount());
            }
        }

        System.exit(0);

    }
}
