package StockExchangeSimulator;

public class Share {
    private String Name;
    private int amount;
    private int price;

    public Share(String name, int amount, int price) {
        Name = name;
        this.amount = amount;
        this.price = price;
    }

    public String getName() {
        return Name;
    }

    public int getAmount() {
        return amount;
    }

    public int getPrice() {
        return price;
    }

    public void changePrice() {
        if((int) ( Math.random() * 2 ) == 1){
            price -= (price*3/100);
        } else {
            price += (price*3/100);
        }
    }

    public void minusAmount(int n){
        amount -= n;
    }

}
